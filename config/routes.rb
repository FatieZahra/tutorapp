Rails.application.routes.draw do

  resources :tutors do 
    resources :ranks
  end

  root "tutors#index"

  get "/aranks" => "ranks#aranks"

end

class AddAgeRangeToTutors < ActiveRecord::Migration
  def change
    add_column :tutors, :age_range, :string
  end
end

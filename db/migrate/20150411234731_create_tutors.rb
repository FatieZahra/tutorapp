class CreateTutors < ActiveRecord::Migration
  def change
    create_table :tutors do |t|
      t.string :name
      t.string :sex
      t.string :state
      t.string :lg

      t.timestamps null: false
    end
  end
end

class CreateRanks < ActiveRecord::Migration
  def change
    create_table :ranks do |t|
      t.integer :rating
      t.text :review
      t.references :tutor, index: true

      t.timestamps null: false
    end
    add_foreign_key :ranks, :tutors
  end
end

class Tutor < ActiveRecord::Base
	has_many :ranks, dependent: :delete_all
	validates :name, presence: true

	def ratingAverage
		if self.ranks.blank?
			ratingAverage = 0
		else
			self.ranks.average(:rating).round(2)
		end
	end
	Age = %w(20-30 30-40 40-50 50+)
end

class RanksController < ApplicationController
  before_action :set_tutor, except: [:aranks]
  before_action :set_rank, only: [:show, :edit, :update, :destroy]

  # GET /ranks
  # GET /ranks.json
  def aranks
    @aranks = Rank.all
  end

  def index
    @ranks = @tutor.ranks.all.order("created_at DESC")
  end

  # GET /ranks/1
  # GET /ranks/1.json
  def show
  end

  # GET /ranks/new
  def new
    @rank = @tutor.ranks.build()
  end

  # GET /ranks/1/edit
  def edit
  end

  # POST /ranks
  # POST /ranks.json
  def create
    @rank = @tutor.ranks.build(rank_params)

    respond_to do |format|
      if @rank.save
        format.html { redirect_to [@tutor, @rank], notice: 'Rank was successfully created.' }
        format.json { render :show, status: :created, location: @rank }
      else
        format.html { render :new }
        format.json { render json: @rank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ranks/1
  # PATCH/PUT /ranks/1.json
  def update
    respond_to do |format|
      if @rank.update(rank_params)
        format.html { redirect_to [@tutor, @rank], notice: 'Rank was successfully updated.' }
        format.json { render :show, status: :ok, location: @rank }
      else
        format.html { render :edit }
        format.json { render json: @rank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ranks/1
  # DELETE /ranks/1.json
  def destroy
    @rank.destroy
    respond_to do |format|
      format.html { redirect_to tutor_ranks_url, notice: 'Rank was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_tutor
      @tutor = Tutor.find(params[:tutor_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_rank
      @rank= @tutor.ranks.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rank_params
      params.require(:rank).permit(:rating, :review)
    end
end

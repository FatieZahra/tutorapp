json.array!(@tutors) do |tutor|
  json.extract! tutor, :id, :name, :sex, :state, :lg
  json.url tutor_url(tutor, format: :json)
end

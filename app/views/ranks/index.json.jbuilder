json.array!(@ranks) do |rank|
  json.extract! rank, :id, :rating, :review, :tutor_id
  json.url rank_url(rank, format: :json)
end
